﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public Transform target;
	public Vector3 zOffset = new Vector3(0,0,-10);
	public float maxSpeed = 5.0f;
	public float maxOffset = 2.0f;

	//private Vector3 oldPos;
	private Rigidbody2D targetRigidbody;

	private Vector3 xyOffset = Vector3.zero;
	public float lerpFactor = 0.5f;

	// Use this for initialization
	void Start () {
		//oldPos = target.position;
		targetRigidbody = target.GetComponent<Rigidbody2D>();
	}

	void Update () {
		/* // compute the target's velocity
		Vector3 velocity = 
			(target.position - oldPos) / Time.deltaTime;
		oldPos = target.position;

		Debug.Log(velocity.magnitude);

		// move the camera in front of the target, 
		// proportional to its velocity
		Vector3 xyOffset = velocity * maxOffset / maxSpeed;
		transform.position = target.position + xyOffset + zOffset;
		*/ 
		//transform.position = target.position + zOffset;

		Vector3 velocity = targetRigidbody.velocity;

		// move the camera in front of the target
		// proportional to its velocity
		Vector3 xyOffset = velocity * maxOffset / maxSpeed;

		// move the camera in front of the target
		// proportional to its velocity
		xyOffset = Vector3.Lerp(
			xyOffset, 
			velocity * maxOffset / maxSpeed, 
			1 - Mathf.Pow(1 - lerpFactor, Time.deltaTime));
		
		transform.position = target.position + xyOffset + zOffset;

	}
}
